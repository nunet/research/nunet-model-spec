# nunet-model-spec

This repository contains ongoing research and resulting specifications of the overall nunet model of computing. The published and constantly update version is here https://nunet.gitlab.io/publisher/technical-whitepaper/.

Our goal is to reach a state of platform development where these specifications are reasonably close to the actual codebase. Besides specifications, we are researching formal descriptive languages, publishing methods, and all that may help us advance.

Resources: 
* It is produced using [R/bookdown](https://bookdown.org/) for easy publishing via different formats as well as to support formulas.
* https://rstudio4edu.github.io/rstudio4edu-book/index.html#why-rstudio4edu
